//
//  Current.swift
//  WeatherX
//
//  Created by SushantNeupane on 9/12/22.
//

import Foundation

struct Current : Codable, Hashable{
    var clouds : Double
    var dew_point : Double
    var dt : UInt64
    var feels_like : Double
    var humidity : Double
    var pressure: Double
    var sunrise : Double
    var sunset : Double
    var temp : Double
    var uvi : Double
    var visibility : Double
    var weather : [Weather]
    var wind_deg : Double
    var wind_speed : Double
}

struct Daily : Codable, Hashable{
    var clouds : Double
    var dt : UInt64
    var sunrise : UInt64
    var sunset : UInt64
    var rain : Double
    var temp : Temp
    var uvi : Double
    var weather : [Weather]
    var wind_deg : Double
    var wind_gust : Double
    var wind_speed : Double
}

struct FeelsLike : Codable, Hashable{
    var day : Double
    var eve : Double
    var morn : Double
    var night : Double
}

struct Hourly : Codable, Hashable{
    var clouds : Double
    var dt : UInt64
    var pressure: Double
    var temp : Double
    var uvi : Double
    var visibility : Double
    var weather : [String]
    var wind_deg : Double
    var wind_gust : Double
    var wind_speed : Double
}

struct LocationWeather : Codable, Hashable{
    var current : Current
    var daily : [Daily]
    var hourly : [Hourly]
    var lat : Double
    var lon : Double
    var timezone : String
    var timezone_offset : Int
}

struct Temp : Codable, Hashable{
    var day : Double
    var eve : Double
    var max : Double
    var min : Double
    var morn : Double
    var night : Double
}

struct Weather : Codable, Hashable{
    var description : String
    var icon : String
    var id : Double
    var main : String
}

struct WeatherX : Codable, Hashable{
    var description : String
    var icon : String
    var id : Double
    var main : String
}

struct WeatherXX : Codable, Hashable{
    var description : String
    var icon : String
    var id : Double
    var main : String
}
