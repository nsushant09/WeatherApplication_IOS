//
//  WeatherXApp.swift
//  WeatherX
//
//  Created by SushantNeupane on 9/12/22.
//

import SwiftUI

@main
struct WeatherXApp: App {
    
    @StateObject var mainViewModel = MainViewModel()
    
    var body: some Scene {
        WindowGroup {
            NavigationView{
                MainView()
            }
            .navigationViewStyle(StackNavigationViewStyle())
            .environmentObject(mainViewModel)
        }
    }
}
